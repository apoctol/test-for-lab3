﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_Logic_Encription
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Тест по обработке символов помимо кирилицы пройден:"+CharEnc_ReturnedNoChanges());
            Console.WriteLine("Тест по обработке символов кирилицы пройден:" + CharEnc_ChangesExcists());
            Console.WriteLine("Тест на корректное смещение знаков кирилицы пройден:" + CharEnc_ChangesCorrect());
            //char b = Encoding.GetEncoding(1251).GetString(new byte[] { 49 })[0];
            //Console.WriteLine(b);
            Console.ReadKey();
        }
        static bool CharEnc_ReturnedNoChanges()
        {
            Random r = new Random();
            bool testOK=true;
            try
            {
                for (int i = 0; i < 1000; i++)
                {
                    int a = r.Next();
                    char b = (char)r.Next(0, 192);
                    char c = Convert.ToChar(Test_Emp.Program.CharEncripter_Test(a, b));
                    if (b != c)
                    {
                        testOK = false;
                        break;
                    }
                }
            }
            catch (Exception)
            {

                testOK = false;
            }
            return testOK;
        }
        static bool CharEnc_ChangesExcists()
        {
            Random r = new Random();
            bool testOK = true;
            try
            {
                for (int i = 0; i < 1000; i++)
                {
                    int a = r.Next(1,33);
                    byte k = (byte)r.Next(192, 256);
                    char b = Encoding.GetEncoding(1251).GetString(new byte[] { k })[0]; //(char)(r.Next(128, 160));
                    char c = Convert.ToChar(Test_Emp.Program.CharEncripter_Test(a, b));
                    if (b == c)
                    {
                        testOK = false;
                        break;
                    }
                }
            }
            catch (Exception)
            {

                testOK = false;
            }
            return testOK;
        }
        static bool CharEnc_ChangesCorrect()
        {
            Random r = new Random();
            bool testOK = true;
            try
            {
                for (int i = 0; i < 1000; i++)
                {
                    int a = 1;
                    byte k = (byte)r.Next(192, 256);
                    char b = Encoding.GetEncoding(1251).GetString(new byte[] { k })[0]; //(char)(r.Next(128, 160));
                    char c = Convert.ToChar(Test_Emp.Program.CharEncripter_Test(a, b));
                    //b = Convert.ToChar(b.ToString().ToLower());
                    //c = Convert.ToChar(c.ToString().ToLower());
                    if ((int)c - (int)b == 1|| (int)c - (int)b == -31 || (int)c - (int)b == -20 || (int)c - (int)b == 28)
                    {
                        testOK = true;
                    }
                    else
                    {
                        Console.WriteLine((int)b);
                        Console.WriteLine(b);
                        Console.WriteLine((int)c);
                        Console.WriteLine(c);
                        Console.WriteLine(c - b);
                        Console.WriteLine("");
                        testOK = false;
                        break;
                    }
                    
                }
            }
            catch (Exception x)
            {
                testOK = false;
                Console.WriteLine("Выпал Exeption"+x.ToString());
            }
            return testOK;
        }
    }
}
